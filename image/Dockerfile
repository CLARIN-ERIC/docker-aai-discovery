FROM registry.gitlab.com/clarin-eric/docker-tomcat8:1.2.0

#Update tomcat server configuration
COPY server.xml /etc/tomcat8/server.xml

#Remove default webapp
RUN rm -r /var/lib/tomcat8/webapps/ROOT

#Download and extract binaries
COPY release-1.9.3.tar.gz /tmp/release.tar.gz
RUN cd /tmp \
 && tar -xf /tmp/release.tar.gz \
 && mkdir -p /var/lib/tomcat8/webapps/discovery \
 && mkdir -p /var/lib/tomcat8/webapps/metadata-api \
 && cd /var/lib/tomcat8/webapps/discovery \
 && unzip /tmp/discovery-service-1.9.3.war \
 && cd /var/lib/tomcat8/webapps/metadata-api \
 && unzip /tmp/metadata-api-1.9.3.war \
 && rm /tmp/*.war \
 && rm /tmp/*.tar.gz

COPY webapp/discovery/web.xml /var/lib/tomcat8/webapps/discovery/WEB-INF/web.xml
COPY webapp/metadata-api/web.xml /var/lib/tomcat8/webapps/metadata-api/WEB-INF/web.xml
COPY convert_metadata.sh /usr/bin/convert_metadata.sh
COPY input.xml /var/lib/tomcat8/webapps/input.xml
COPY GeoLite2-City.mmdb /data/GeoLiteCity.dat
RUN chmod u+x /usr/bin/convert_metadata.sh

COPY entrypoint.sh /usr/bin/entrypoint.sh
RUN chmod u+x /usr/bin/entrypoint.sh

USER root

RUN mkdir -p /data
ADD idps.json /data/idps.json
RUN chown -R tomcat8 /data

VOLUME ["/data"]

ENTRYPOINT ["/usr/bin/entrypoint.sh"]
CMD ["--tomcat"]