#!/bin/bash

set -e

MD_API_SCHEME=${API_SCHEME:-"https"}
MD_API_HOST=${API_HOST:-"localhost"}
MD_API_PORT=${API_PORT:-"8443"}

echo "scheme=${MD_API_SCHEME}, host=${MD_API_HOST}, port=${MD_API_PORT}"

curl -k -f -L -o /data/idps.json -X POST -d @input.xml -H"Content-Type: application/xml" -H"Accept: application/json" ${MD_API_SCHEME}://${MD_API_HOST}:${MD_API_PORT}/metadata-api/rest/metadata/discojuice
