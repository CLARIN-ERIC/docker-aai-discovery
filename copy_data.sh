#!/bin/bash

VERSION="1.9.3"
REMOTE_RELEASE_URL="https://github.com/clarin-eric/aai-discovery/releases/download/release-1.9.3/release-1.9.3.tar.gz"

init_data () {
    LOCAL=0
    if [ "$1" == "local" ]; then
        LOCAL=1
    fi

    if [ "${LOCAL}" -eq 0 ]; then
        echo -n "Fetching remote data"
        curl -s -S -J -L -O "${REMOTE_RELEASE_URL}"
    else
        echo -n "Fetching local data"
        cd image
        mkdir tmp
        cd tmp
        cp "/Users/wilelb/.m2/repository/eu/clarin/aai/discovery-service/${VERSION}/discovery-service-${VERSION}.war" .
        cp "/Users/wilelb/.m2/repository/eu/clarin/aai/discovery-metadata-api/${VERSION}/discovery-metadata-api-${VERSION}.war" .
        mv "discovery-metadata-api-${VERSION}.war" "metadata-api-${VERSION}.war"

        tar -pczvf "../release-${VERSION}.tar.gz" \
            "discovery-service-${VERSION}.war" \
            "metadata-api-${VERSION}.war"
        cd ..
        rm -r tmp

        cd ..
    fi
}

cleanup_data () {
    if [ -f "image/release-${VERSION}.tar.gz" ]; then
        echo -n "Cleaning data"
        rm image/release-${VERSION}.tar.gz
    fi
}